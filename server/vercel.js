const { createPageRenderer } = require('vite-plugin-ssr')
const createHandler = require('../server/createHandler')
require('../dist/server/importBuild')

const pageRenderer = createPageRenderer({ isProduction: true })

module.exports = createHandler({ pageRenderer })
