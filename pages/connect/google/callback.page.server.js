import { getQuery, withQuery } from 'ufo'
import fetch from 'node-fetch'

export const onBeforeRender = async pageContext => {
  const { session, setSession, origin } = pageContext
  const query = getQuery(pageContext.url)

  if (query.code) {
    const tokenUrl = withQuery('https://oauth2.googleapis.com/token', {
      client_id: pageContext.env.GOOGLE_CLIENT_ID,
      client_secret: pageContext.env.GOOGLE_CLIENT_SECRET,
      code: query.code,
      grant_type: 'authorization_code',
      redirect_uri: `${origin}/connect/google/callback`,
    })
    const { access_token: accessToken } = await fetch(tokenUrl, {
      method: 'POST',
    }).then(resp => resp.json())
    const userInfo = await fetch(
      'https://openidconnect.googleapis.com/v1/userinfo',
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    ).then(resp => resp.json())

    session.user = userInfo
    setSession(session)
  }

  return {
    pageContext: {
      redirectTo: '/',
    },
  }
}
