// import fetch fr0om 'node-fetch'

export const onBeforeRender = pageContext => {
  const { session, setSession } = pageContext
  session.message = 'Hello from index page'
  setSession(session)
  const user = pageContext.user
  return {
    pageContext: {
      pageProps: {
        user,
      },
    },
  }
}
