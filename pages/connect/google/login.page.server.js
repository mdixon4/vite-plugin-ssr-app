import { withQuery } from 'ufo'

export const onBeforeRender = pageContext => {
  const { session, setSession, env, origin } = pageContext

  const location = withQuery('https://accounts.google.com/o/oauth2/v2/auth', {
    client_id: env.GOOGLE_CLIENT_ID,
    redirect_uri: `${origin}/connect/google/callback`,
    response_type: 'code',
    scope: 'openid profile email',
    state: 'next=/',
  })

  return {
    pageContext: {
      redirectTo: location,
    },
  }
}
