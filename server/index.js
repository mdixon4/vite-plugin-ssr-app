const express = require('express')
const { createPageRenderer } = require('vite-plugin-ssr')
const createHandler = require('./createHandler')

const dotenv = require('dotenv')
dotenv.config()

const isProduction = process.env.NODE_ENV === 'production'
const root = `${__dirname}/..`
if (isProduction) require('../dist/server/importBuild')

startServer()

async function startServer() {
  const app = express()
  app.use(express.urlencoded({ extended: false }))
  app.use(express.json())
  app.use(express.text())

  let viteDevServer
  if (isProduction) {
    app.use(express.static(`${root}/dist/client`))
  } else {
    const vite = require('vite')
    viteDevServer = await vite.createServer({
      root,
      server: { middlewareMode: 'ssr' },
    })
    app.use(viteDevServer.middlewares)
  }

  const pageRenderer = createPageRenderer({ root, isProduction, viteDevServer })
  const handler = createHandler({ pageRenderer })
  app.use('*', handler)

  const port = process.env.PORT || 3000
  app.listen(port)
  console.log(`Server running at http://localhost:${port}`)
}
