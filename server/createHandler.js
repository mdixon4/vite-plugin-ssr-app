// const { Cookie } = require('tough-cookie')
const base64 = require('base-64')
const Cookies = require('cookies')
const Keygrip = require('keygrip')
const { text } = require('micro')
const { provideTelefuncContext, telefunc } = require('telefunc')
const { $URL } = require('ufo')

const getSession = cookies => {
  const sessionCookie = cookies.get('session', { signed: true })
  let session = {}
  try {
    session = JSON.parse(base64.decode(sessionCookie))
  } catch (err) {
    console.warn('Could not parse session cookie', sessionCookie)
  }
  return session
}

const setSession = (cookies, session) => {
  const sessionCookie = base64.encode(JSON.stringify(session))
  cookies.set('session', sessionCookie, { signed: true })
}

const getOrigin = req => {
  const origin = new $URL()
  origin.host = req.headers.host
  origin.protocol = origin.host.startsWith('localhost') ? 'http:' : 'https:'
  return origin.toString()
}

module.exports = function createHandler({ pageRenderer }) {
  return async function handler(req, res) {
    const origin = getOrigin(req)
    const keys = new Keygrip([process.env.COOKIE_SECRET_KEY])
    const cookies = new Cookies(req, res, { keys })
    const session = getSession(cookies)
    const user = session.user
    const url = req.originalUrl || req.url
    const method = req.method
    const body =
      req.body ||
      (req.headers['content-type'] &&
        req.headers['content-type'].startsWith('text/') &&
        (await text(req)))

    if (url.startsWith('/_telefunc')) {
      provideTelefuncContext({ user })
      const httpResponse = await telefunc({ url, method, body })
      if (httpResponse) {
        const { statusCode, contentType, body } = httpResponse
        res.statusCode = statusCode
        res.setHeader('content-type', contentType)
        res.end(body)
        return
      }
    }

    const { httpResponse, redirectTo } = await pageRenderer({
      origin,
      url,
      session,
      setSession: session => setSession(cookies, session),
      env: {
        GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
        GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
      },
      user,
    })

    if (redirectTo) {
      console.log('redirectTo', res, res.redirect)
      res.statusCode = 302
      res.setHeader('location', redirectTo)
      res.end()
    } else if (!httpResponse) {
      res.statusCode = 200
      res.end()
    } else {
      const { body, statusCode, contentType } = httpResponse

      res.statusCode = statusCode
      res.setHeader('content-type', contentType)
      res.end(body)
    }
  }
}
